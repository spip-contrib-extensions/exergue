<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'exergue_description' => 'Permet de reprendre un paragraphe d’un article pour le mettre en évidence. Il s’agit d’une phrase du texte que l’on fait ressortir dans la maquette, histoire de rythmer la page et de faire ressortir un point accrocheur. Le contenu est duppliqué en Javascript. On peut choisir la zone de duplication explicitement avec <code>[exergueNNN<-]</code> où NNN est le numéro d’ordre de l’exergue concernée.',
	'exergue_nom' => 'Exergue',
	'exergue_slogan' => 'Gérer des exergues : <code><exergue></exergue></code> et <code>[exergueNNN<-]</code>'
);
